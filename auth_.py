"""ISIDOREbot : a twitter bot that responds to a tweet
of the form : "Dis isidore, qu'as-tu sur Prénom Nom ?"

Usage:
======
    python2 bot.py

"""

__authors__ = ("Stéphane Pouyllau")
__contact__ = ("stephane.pouyllau@huma-num.fr")
__copyright__ = "CeCILL-C"
__date__ = "2022-01-01"
__version__ = "0.1"


import tweepy


def auth():
    # Keys
    consumer_key = "XXXXXXXX"
    consumer_secret = "XXXXXXXX"

    # Access_token
    access_token = "XXXXXXXX"
    access_token_secret = "XXXXXXXX"

    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    api = tweepy.API(auth)
    return api, auth
