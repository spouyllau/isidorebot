"""ISIDOREbot : a twitter bot that responds to a tweet
of the form : "Dis isidore, qu'as-tu sur Prénom Nom ?"

Usage:
======
    python2 bot.py

"""

__authors__ = ("Stéphane Pouyllau")
__contact__ = ("stephane.pouyllau@huma-num.fr")
__copyright__ = "CeCILL-C"
__date__ = "2022-01-01"
__version__ = "0.1"

from tweepy import Stream
from tweepy.streaming import StreamListener
import auth
import json
import unidecode


# Class "listener" : listen tweet with filter


class listener(StreamListener):
    def on_data(self, data):
        all_data = json.loads(data)

        id_tweet = all_data['id_str']

        tweet = all_data['text']

        username = all_data['user']['screen_name']

        text = analyzer(tweet, username)

        if not contains_word(tweet, 'RT') and not contains_word(tweet, 'exemple'):
            print(username, tweet)
            api.update_status(
                status='@'+ username +' '+ text,
                in_reply_to_status_id=id_tweet,
                #in_reply_to_status_id_str=id_tweet,
                possibly_sensitive='false'
            )

        return True

    def on_error(self, status):
        print(status)

# Def "analyzer" : analyze the tweet to extract name and fist name to
# construct URLs for resquests on ISIDORE


def analyzer(tweet, username):
    # Name and fist name
    L = tweet.split()
    first, *middle, last = tweet.split()
    print(tweet)
    word_maj = []

    for word in L:
        if (word[0].isupper() and word[1].islower()):
            if word != first:
                word_maj.append(word)

    prenom = word_maj[0]
    nom = word_maj[1]

    prenom4api = word_maj[0].lower().replace("-", "_")
    prenom4api = unidecode.unidecode(prenom4api)
    prenom4url = unidecode.unidecode(prenom)

    nom4api = word_maj[1].lower()
    nom4url = unidecode.unidecode(nom)

    # Make differents good URLs for ISIDORE
    url = "https://isidore.science/s?q=author%3A%22"+prenom4url+"+"+nom4url+"%22"

    # Make the answer for the original tweet
    text = "Bonjour, peut-être devriez-vous regarder sur ISIDORE la page https://isidore.science/a/"+nom4api+"_"+prenom4api+" ou ses principaux travaux sur "+url+"."

    return(text)

# Def "contains_word" : detect contains words


def contains_word(s, w):
    return (' ' + w + ' ') in (' ' + s + ' ')


# Initialize Twitter API connexion
api, auth = auth.auth()


# The bot
twitterStream = Stream(auth, listener())
twitterStream.filter(track=["disidore", "dis @isidorebot___", "hey isidore"])
